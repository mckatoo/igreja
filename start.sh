#/bin/bash

PROJECT=`pwd | sed 's#.*/##'`

docker build -t mckatoo/$PROJECT .

docker stack deploy -c docker-compose.yml $PROJECT
